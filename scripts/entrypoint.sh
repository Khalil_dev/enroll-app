#!/bin/sh
#!/bin/bash

aws configure set region $AWS_DEFAULT_REGION
#aws ssm get-parameter --name "$AWS_PARAMETER_STORE" --with-decryption > /app/aws_param_store.json
set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate



uwsgi --socket :9000 --workers 4 --master --enable-threads --module yup_enroll.wsgi