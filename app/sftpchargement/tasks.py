import time
import requests
from celery import shared_task
from core.models import SFTPServerParam
from . import serializers
import shutil
import tempfile
import urllib.request
import paramiko



class TokenRefresh():
    """Class used for getting Token from TagPay """
    host = None
    access_token = None
    access_token_expiration = None

    def __init__(self,host='https://33021.tagpay.fr/api/v2/admin/oauth2/token'):
        # the function that is executed when
        # an instance of the class is created
        self.host = host

        try:
            self.access_token = self.getAccessToken()
            if self.access_token is None:
                raise Exception ("Request for access token failed")
        except Exception as e:
            print(e)
        else:
            self.access_token_expiration = time.time()+10800

    def getAccessToken(self):
        # the function that is 
        # used to request the JWT
        try:
            request_body ={
                "grant_type":"client_credentials",
                "client_id":"Odoo",
                "client_secret":"2e6e9510-1c73-4bf9-8349-6c8f0ed400b4",
                "scope":"ClientView MerchantView AgentView TransactionView",

            }
            headers = {
                'Content-Type':'application/json'
            }
            request = requests.post(self.host, data=request_body, headers=headers)

            request.raise_for_status()
        except Exception as e:
            print(e)
            return None
        else:
            #Refering to response structure from TagPay Documentation
            # {"access_token": ""}
            return requests.json()['access_token']

    class Decorators():
        @staticmethod
        def refreshToken(decorated):
            #the function that used to check
            #the JWT and refresh if necessary
            def wrapper(api,*args,**kwargs):
                if time.time() > api.access_token_expiration:
                    self.getAccessToken()
                return decorated(api,*args,**kwargs)
            return wrapper


@shared_task
def connecting_sftp_server(server_id):
    """ Asynchronus task for connecting to sftp server """
    target_server = SFTPServerParam.objects.get(id=server_id)
    serialized = serializers.SFTPParamSerializer(target_server)
    processing_data = serialized.data

    host = processing_data['nom_machine']
    port = int(processing_data['port'])
    username = processing_data['username']
    password = processing_data['password']