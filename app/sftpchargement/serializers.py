from rest_framework import serializers
from core.models import SFTPServerParam


class SFTPParamSerializer(serializers.ModelSerializer):
    """ Creating a Serializer for SFTP Param Server"""

    class Meta:
        model = SFTPServerParam
        fields = "__all__"
        read_only_fields = ('id', )


class ConfirmationSerializer(serializers.Serializer):
    """ Recieve confirmation for SFTP Loading csv to TagPay Endpoint """

    confirmation = serializers.BooleanField(default=False)