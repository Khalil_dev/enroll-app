from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url

router = DefaultRouter()

router.register('sftpserverparams', views.CreateSFTPParamViewSet, basename='sftpserverparams')

urlpatterns = [
    path('', include(router.urls))
]
