from django.shortcuts import render
from rest_framework import viewsets, mixins
from core.models import SFTPServerParam
from rest_framework.decorators import action
from rest_framework.response import Response
from celery.result import AsyncResult
from yup_enroll.celery import app
from django import forms
from db_file_storage.form_widgets import DBClearableFileInput


from .tasks import connecting_sftp_server


from . import serializers



class CreateSFTPParamViewSet(viewsets.GenericViewSet,
                            mixins.ListModelMixin,
                            mixins.CreateModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin,
                            ):
    
    """ ViewSet for Handling SFTP ServerParam """
    queryset = SFTPServerParam.objects.all()
    serializer_class = serializers.SFTPParamSerializer


    @action(detail=True, methods=['POST'], url_path="sftp-confirm", serializer_class=serializers.ConfirmationSerializer)
    def confirm_sftp_chargement(self, request, pk=None):
        serialized = serializers.ConfirmationSerializer(request.data)
        param_server_instance = self.get_object()
        param_server_serialized = serializers.SFTPParamSerializer(param_server_instance)
        current_param_id = param_server_serialized.data['id']
        if serialized.data['confirmation'] == False:
            return Response({
                'message': 'Chargement non confirmé',
                'id_current_instance': current_param_id 
            })
        if serialized.data['confirmation'] == True:
            # lunching asynchronus task
            connecting_sftp_server.delay(current_param_id)
            return Response({
                'message': 'Operation de chargement automatique déclenchée! ',
                'id_current_instance': current_param_id
            })


class SFTPServerParamForm(forms.ModelForm):
    class Meta:
        model = SFTPServerParam
        exclude = []
        widgets = {
            'id': DBClearableFileInput
        }




