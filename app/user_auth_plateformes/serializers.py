from rest_framework import serializers
from core.models import UserAuthorizedPlateforme


class UserAuthorizedPlateforme(serializers.ModelSerializer):
    """ Create a serializer for PlateformeUserAuthorization"""

    class Meta:
        model = UserAuthorizedPlateforme
        fields = "__all__"
        read_only_fields = ('id',)

