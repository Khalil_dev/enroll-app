from rest_framework import viewsets, mixins
from core.models import UserAuthorizedPlateforme
from user_auth_plateformes import serializers

class UserAuthorizedPlateforme(viewsets.GenericViewSet,
                                mixins.CreateModelMixin,
                                mixins.ListModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.DestroyModelMixin):
    
    """ ViewSet for handling UserAuthorizedPlateformes """
    queryset = UserAuthorizedPlateforme.objects.all()
    serializer_class = serializers.UserAuthorizedPlateforme