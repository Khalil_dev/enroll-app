from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()

router.register('userauthorizedplateforms', views.UserAuthorizedPlateforme)
app_name = 'userauthorizedplateformes'


urlpatterns = [
    path('', include(router.urls))
]
