from django.apps import AppConfig


class UserAuthPlateformesConfig(AppConfig):
    name = 'user_auth_plateformes'
