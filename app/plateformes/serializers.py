from rest_framework import serializers
from core.models import Plateforme

class PlateformeSerializer(serializers.ModelSerializer):
    """ Serializer for plateformes objects """

    class Meta:
        model = Plateforme
        fields ='__all__'
        read_only_fields = ('id',)