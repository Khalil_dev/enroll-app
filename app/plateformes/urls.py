from django.urls import path, include
from rest_framework.routers import DefaultRouter

from plateformes import views


router = DefaultRouter()
router.register('plateforms', views.PlatefomresViewSet)

app_name = 'plateformes'

urlpatterns = [
    path('', include(router.urls))
]
