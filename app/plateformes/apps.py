from django.apps import AppConfig


class PlateformesConfig(AppConfig):
    name = 'plateformes'
