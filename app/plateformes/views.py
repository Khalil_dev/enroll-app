from rest_framework import viewsets, mixins
from core.models import Plateforme
from plateformes import serializers
from rest_framework.pagination import PageNumberPagination

class StandarResultSetsPagination(PageNumberPagination):
    page_size_query_param = 2
    max_page_size = 1000



class PlatefomresViewSet(viewsets.GenericViewSet, 
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin):
    pagination_class = StandarResultSetsPagination

    """ Manage plateformes in the database """
    queryset = Plateforme.objects.all()
    serializer_class = serializers.PlateformeSerializer

