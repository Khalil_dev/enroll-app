from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from core import models
from django.utils.translation import gettext as _

class UserAdmin(BaseUserAdmin):
    ordering = ['id']
    list_display = ['email','nom', 'prenom', 'is_active']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Informations personnelles'), {'fields': ('prenom', 'nom',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        (_('Historique de Dates'), {'fields': ('last_login',)})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'prenom', 'nom', 'password1', 'password2','is_active', 'plateformes')
        }),
    )

class ChargementAdmin(BaseUserAdmin):
    pass



class PlateformeAdmin(BaseUserAdmin):
    """ """
    ordering = ['id']
    list_display = ['nom_plateforme','endpoint_api', 'api_key']

class RoleAdmin(BaseUserAdmin):
    list_display = [
        'nom_role'
    ]

admin.site.register(models.User, UserAdmin)
admin.site.register(models.Role)
admin.site.register(models.Plateforme)
admin.site.register(models.UserAuthorizedPlateforme)
admin.site.register(models.UserRole)
admin.site.register(models.ProfileClient)
admin.site.register(models.Chargement)
