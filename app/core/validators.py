import csv
import os
import io

from django.core.exceptions import ValidationError

REQUIRED_HEADER = [
    'client_id', 
    'client_status', 
    'client_name', 
    'client_firstName',
    'client_msisdn',
    'client_balance',
    'client_cos'
    ]

def csv_file_validator(value):
    filename, ext  = os.path.splitext(value.name)
    if str(ext) != '.csv':
        raise ValidationError("Le document doit être au format CSV")
    return True
    
    # decoded_file = value.read().decode('utf-8')
    # io_string = io.StringIO(decoded_file)
    # reader = csv.reader(io_string, delimiter=';', quotechar='|')
    # header_ =next(reader)[0].split(',')
    # if header_[1]=='':
    #     header_.pop()
    # required_header = REQUIRED_HEADER
    # if required_header != header_:
    #     raise ValidationError("Fichier invalid. Veuillez utiliser un fichier CSV avec des entêtes valides")

