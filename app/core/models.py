from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, \
                                        PermissionsMixin
from django.conf import settings
from . import validators
import encodings
from db_file_storage.model_utils import delete_file, delete_file_if_needed



class Role(models.Model):
    """ Model for role management in system """
    name = models.CharField(max_length=255)


class UserManager(BaseUserManager):
    """ User Management """
    def create_user(self, email, password=None, **extra_fields):
        """ Create and save a new user """
        if not email:
            raise ValueError('Adresse email requise')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """ Creates and saves a super user """
        user = self.create_user(email, password)
        user.is_staff = True 
        user.is_superuser= True
        user.save(using=self._db)
        
        return user
class ProfileClient(models.Model):
    """ Creating a client profile """
    nom_profile = models.CharField(max_length=255, unique=True)

class Plateforme(models.Model):
    """ Plateforme Representation """
    nom_plateforme = models.CharField(max_length=255)
    endpoint_api = models.URLField(max_length=255, unique=True)
    client_id = models.IntegerField()
    api_key = models.CharField(max_length=255)
    profile_client_disponible = models.ManyToManyField(ProfileClient, through='AvailableProfilePlateformes')

class AvailableProfilePlateformes(models.Model):
    """ Handle available plateformes """
    profile_client = models.ForeignKey(ProfileClient, on_delete=models.CASCADE)
    plateforme = models.ForeignKey(Plateforme, on_delete=models.CASCADE)


class User(AbstractBaseUser, PermissionsMixin):
    """ Custom user model that supports using email instead of username for authentication """

    email = models.EmailField(max_length=255, unique=True)
    prenom = models.CharField(max_length=255, unique=True)
    nom = models.CharField(max_length=255)
    roles = models.ManyToManyField(Role, through='UserRole')
    is_active = models.BooleanField(default=True, )
    is_staff = models.BooleanField(default=True)
    plateformes = models.ManyToManyField(Plateforme, through='UserAuthorizedPlateforme')

    objects = UserManager()

    USERNAME_FIELD = 'prenom'

class UserRole(models.Model):
    """Handle user management """
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class UserAuthorizedPlateforme(models.Model):
    """ Defining intermediare class between  users and their associated plateforms """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    plateforme = models.ForeignKey(Plateforme, on_delete=models.CASCADE)



def upload_csv_file(instance, filename):
    """ Function for creating and storing uploaded file on fileSystem """
    qs = instance.__class__.objects.filter(user=instance.user)
    if qs.exists():
        num_ = qs.last().id + 1
    else:
        num_ = 1
    return f'csv/{num_}/{instance.user.prenom}/{filename}'

class ChargementFile(models.Model):
    """ Model for storing a file in database using django_db_storage """
    bytes = models.BinaryField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=10)


class Chargement(models.Model):
    """ loading enrollement File"""
    plateforme = models.ForeignKey('Plateforme', on_delete=models.CASCADE)
    profil = models.ForeignKey('ProfileClient', on_delete=models.PROTECT)
    encodage = models.CharField(choices=[(f'{e}', e) for e in encodings.aliases.aliases.values()], max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    presence_entete = models.BooleanField()
    caratere_separation = models.CharField(max_length=1)
    caratere_protection = models.CharField(max_length=1)
    fichier_chargement = models.FileField(upload_to='core.ChargementFile/bytes/filename/mimetype', validators=[validators.csv_file_validator])
    debut_chargement = models.DateTimeField(auto_now_add=True, blank=True)
    completed = models.BooleanField(default=False, blank=True)

    def save(self, *args, **kwargs):
        delete_file_if_needed(self, 'id')
        super(Chargement, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(Chargement, self).delete(*args, **kwargs)
        delete_file(self, 'id')

class MappingFile(models.Model):
    """ Model for storing mapping file in database using django_db_storage """
    bytes = models.BinaryField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=10)

class Mapping(models.Model):
    """ Mapping Table """
    chargement_instance = models.ForeignKey('Chargement', on_delete=models.CASCADE)
    fichier_mapping = models.FileField(upload_to='core.MappingFile/bytes/filename/mimetype')

    def save(self, *args, **kwargs):
        delete_file_if_needed(self, 'id')
        super(Mapping, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(Mapping, self).delete(*args, **kwargs)
        delete_file(self, 'id')

class SFTPFile(models.Model):
    """ Model for storing ssh_key file in database using django_db_storage """
    bytes = models.BinaryField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=10)


class SFTPServerParam(models.Model):
    """ Parameters for auto enroll files loading from SFTP SERVER """
    nom_machine = models.CharField(max_length=255)
    port = models.IntegerField()
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    ssh_key = models.FileField(upload_to='core.SFTPFile/bytes/filename/mimetype', blank=True)
    deposit_path = models.CharField(max_length=255)
    processing_path = models.CharField(max_length=255)
    completed_path = models.CharField(max_length=255)
    results_path = models.CharField(max_length=255)
    recovery_frequence = models.IntegerField()
    format_file = models.CharField(max_length=10)
    plateforme = models.ForeignKey('Plateforme', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        delete_file_if_needed(self, 'id')
        super(SFTPServerParam, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(SFTPServerParam, self).delete(*args, **kwargs)
        delete_file(self, 'id')


