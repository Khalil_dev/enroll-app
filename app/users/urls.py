from django.urls import path, include
from users import views
from rest_framework.routers import DefaultRouter


app_name = 'user'

urlpatterns = [
    path('create/', views.CreateUserView.as_view(), name='create'),
    path('allusers/', views.ListUserView.as_view(), name='allusers'),
    path('myprofile/', views.ManageUserViewSet.as_view(), name='myprofile') 
]
