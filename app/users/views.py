from users.serializers import UserSerializer
from rest_framework import generics, viewsets, mixins
from rest_framework.response import Response
from core.models import User


class CreateUserView(generics.CreateAPIView):
    """ Create a new user in the system """
    serializer_class = UserSerializer

class ListUserView(generics.ListAPIView):
    """ """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ManageUserViewSet(generics.RetrieveUpdateAPIView):
    """ Manage authenticated user """
    serializer_class = UserSerializer

    def get_object(self):
        """ Retrieve an authenticated user """
        return self.request.user

