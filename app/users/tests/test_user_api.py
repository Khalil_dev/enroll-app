#This test cla
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status


CREATE_USER_URL = reverse('user:create')
TOKEN_URL = reverse('user:token')


#Helper function 
def create_user(**params):
    return get_user_model().objects.create_user(**params)

class PublicUserApiTests(TestCase):
    """Test the users API Client """

    def setup(self):
        self.client = APIClient()

    def test_create_valid_user_success(self):
        """ Test creating user with valid payload is successful """
        payload ={
            'email': 'testapiuser@yupenroll.com',
            'password': 'passer123'
        } 

        res = self.client.post(CREATE_USER_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(**res.data)
        self.assertTrue(user.check_password(payload['password']))
        self.assertNotIn('password', res.data)

    
    def test_user_exists(self):
        """Test if a user already exists in database """

        payload = {'email':'yupadmin@yupenroll.com', 'password':'passer123'}
        create_user(**payload)

        res = self.client.post(CREATE_USER_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    
    def test_password_too_short(self):
        """ Test that password is more than 5 charachters"""

        payload ={ 'email': 'yupadmin@yupenroll.com', 'password':'pwd'}

        res = self.client.post(CREATE_USER_URL, payload)


        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        user_exists = get_user_model().objects.filter(
            email=payload['email'],
        ).exists()
        self.assertFalse(user_exists)

    def test_create_token_for_user(self):
        """Test that token is created for the user """
        payload = {'email': 'test@yupenroll.com', 'password': 'passer123'}
        create_user(**payload)
        res = self.client.post(TOKEN_URL, payload)

        sefl.assertIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_201_OK)

    def test_create_token_invalid_creadentials(self):
        """ Test that token is not created if invalid creadentials are given """
        create_user({'email': 'test@yupenroll.com', 'password': 'passer123'})
        payload = {'email':'test@yupenroll.com', 'password':'wrong'}
        res = self.client.post(TOKEN_URL, payload)

        sefl.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


    def test_create_token_no_user(self):
        """Test that no token is created if user doesn't exists """
        payload = {'email':'test@yupenroll.com', 'password':'passer123'}

        res = self.client.post(TOKEN_URL, payload)

        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTPP_400_BAD_REQUEST)

    def test_create_token_missing_filed(self):
        """ Tests that email and password are required """
        res = self.client.post(TOKEN_URL, {'email':'one', 'password':''})

        self.assertNotIn('token',res.data)
        self.assertEqual(res.status_code, status.HTPP_404_BAD_REQUEST)
