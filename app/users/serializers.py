from django.contrib.auth import get_user_model


from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    """ Serializer for the user object """

    class Meta:
        model = get_user_model()
        fields = ('prenom', 'nom','email','password','is_active','plateformes', 'roles', 'is_superuser')
        extra_kwargs = {'password': {'write_only': True, 'min_length':5}}

    def create(self, validated_data):
        """ Create a new user with encrypted password and return it """
        return get_user_model().objects.create_user(**validated_data)


    def update(self, instance, validated_data):
        """Update a user"""
        nom = validated_data.pop('nom', None)
        prenom = validated_data.pop('prenom', None)
        is_active = validated_data.pop('is_active', None)
        user = super().update(instance, validated_data)

        if nom or prenom or is_active:
            user.set_nom(nom)
            user.set_prenom(prenom)
            user.set_is_active(is_active)
            user.save()

        return user

        user = super().update(instance, validated_data)