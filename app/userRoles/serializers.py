from rest_framework import serializers
from core.models import UserRole


class UserRolesSerializers(serializers.ModelSerializer):
    """ Creating a serializer """

    class Meta:
        model = UserRole
        fields = "__all__"
        read_only_fields = ('id',)
