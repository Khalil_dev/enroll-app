from rest_framework import viewsets, mixins
from core.models import UserRole
from . import serializers

class UserRoleViewSet(viewsets.GenericViewSet,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin):
    """ User roles attributions Management """
    serializer_class = serializers.UserRolesSerializers
    queryset = UserRole.objects.all()
