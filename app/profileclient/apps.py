from django.apps import AppConfig


class ProfileclientConfig(AppConfig):
    name = 'profileclient'
