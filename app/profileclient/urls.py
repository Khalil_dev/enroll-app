from django.urls import path, include
from rest_framework.routers import DefaultRouter
from profileclient import views

router = DefaultRouter()

router.register('profile-client', views.ProfileClientViewSet)

urlpatterns = [
    path('', include(router.urls))
]
