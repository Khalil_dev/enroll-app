from rest_framework import viewsets, mixins
from core.models import ProfileClient
from profileclient import serializers

class ProfileClientViewSet(viewsets.GenericViewSet,
                            mixins.CreateModelMixin,
                            mixins.ListModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin):
    """ ProfileClient Viewset """
    queryset = ProfileClient.objects.all()
    serializer_class = serializers.ProfileClientSerializer