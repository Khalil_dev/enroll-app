from rest_framework import serializers
from core.models import ProfileClient

class ProfileClientSerializer(serializers.ModelSerializer):
    """ Serializer for profileClient """

    class Meta:
        model = ProfileClient
        fields = "__all__"
        read_only_fields = ('id',)
        