import os
from celery import Celery


# Setting the default Django settings module to 'celery' program
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'yup_enroll.settings')

#Creating celery instance
app = Celery('yup_enroll')


#Telling Celery to auto-discove asynchronus task for our application
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
