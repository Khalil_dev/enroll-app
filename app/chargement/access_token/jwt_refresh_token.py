import time
import requests

class TokenRefresh():
    host = None
    access_token = None
    access_token_expiration = None

    def __init__(self,host):
        # the function that is executed when
        # an instance of the class is created
        self.host = host

        try:
            self.access_token = self.getAccessToken()
            if self.access_token is None:
                raise Exception ("Request for access token failed")
        except Exception as e:
            print(e)
        else:
            self.access_token_expiration = time.time()+10800

    def getAccessToken(self):
        # the function that is 
        # used to request the JWT
        try:
            request_body ={
                "grant_type":"client_credentials",
                "client_id":"myApiAdmin",
                "client_secret":"eb5d1477-0dab-4b36-bc3e-9da6d6cc25ba",
                "scope":"TransactionView AgentView AgentCreation",
                "Content-Type":"application/json"

            }
            request = requests.post(self.host, data=request_body)

            request.raise_for_status()
        except Exception as e:
            print(e)
            return None
        else:
            #Refering to response structure from TagPay Documentation
            # {"access_token": ""}
            return requests.json()['access_token']

    class Decorators():
        @staticmethod
        def refreshToken(decorated):
            #the function that used to check
            #the JWT and refresh if necessary
            def wrapper(api,*args,**kwargs):
                if time.time() > api.access_token_expiration:
                    api.getAccessToken()
                return decorated(api,*args,**kwargs)
            return wrapper
