from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url
router =DefaultRouter()
router.register('chargement',views.CreateListChargementViewSet, basename='chargement')


urlpatterns = [
    path('', include(router.urls)),
    url(r'^files/', include('db_file_storage.urls')),
]
