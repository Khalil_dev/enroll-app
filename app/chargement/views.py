from rest_framework.response import Response
from core.models import Chargement
from . import serializers
from rest_framework import viewsets, mixins
from django import forms
from db_file_storage.form_widgets import DBClearableFileInput
from rest_framework.decorators import action
from csv import DictReader
import csv
import shutil
import tempfile
import urllib.request
from .serializers import TagPaySerializer
import io
from .tasks import TokenRefresh, doMapping


class CreateListChargementViewSet(viewsets.GenericViewSet, 
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin):
    """ Viewset for handling Chargement fichier Endpoint """
    queryset = Chargement.objects.all()
    serializer_class = serializers.ChargementSerializer
    
    def get_queryset(self):
        """ Return objects for the current authenticated user only """
        return self.queryset.filter(user=self.request.user).order_by('-id')

    def performe_create(self, serializer):
        """ Allow the current authenticated user to make upload enroll file"""
        serializer.save(user=self.request.user)
#OK
    @action(detail=True, methods=['GET', 'POST'], url_path='headers-processing', serializer_class=serializers.TagPaySerializer)
    def header_checking(self, request, pk=None):
        """ Allow to check headers existing in uploaded file or doing mapping with Tagpay Fields"""
        serializer_class = serializers.TagPaySerializer
        chargement = self.get_object()
        serialized = serializers.MappingSerializer(chargement)
        processing_chargement = serialized.data
        csv_link = processing_chargement['fichier_chargement']
        check_header = processing_chargement['presence_entete']
        delimiter = processing_chargement['caratere_separation']
        encodage = processing_chargement['encodage']
        if check_header:
            with urllib.request.urlopen('http://localhost:8000'+csv_link) as response:
                with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                    shutil.copyfileobj(response, tmp_file)
            with open(tmp_file.name) as file_csv:
                csv_dict_reader = DictReader(file_csv)
                column_names = csv_dict_reader.fieldnames
                nb_fields = len(column_names)
            return Response({'entetes du fichier': column_names, 'nombre de champs':nb_fields, 'delimiter':delimiter})
        else:
            data = request.data
            with urllib.request.urlopen('http://localhost:8000'+csv_link) as response:
                with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                    shutil.copyfileobj(response, tmp_file)
            with open(tmp_file.name, 'r+') as file_csv:
                csv_reader = csv.reader(file_csv)
                header = next(csv_reader)
                for row in csv_reader:
                    counter = len(row)
                generic_fields = [f'Champ: {x+1}' for x in range(counter)]
                fieldnames = [x for x in data.values()]
                mystring = ",".join(fieldnames)
                content = file_csv.read()
                file_csv.seek(0,0)
                file_csv.write(mystring.rstrip('\r\n') + '\n' + content)
                return Response({'generic_fields':generic_fields, 'data':data})

    @action(detail=True, methods=['POST'], url_path='mapping', serializer_class=serializers.TagPaySerializer)
    def mapping(self, request, pk=None):
        chargement = self.get_object()
        serialized = serializers.ChargementSerializer(chargement)
        processing_chargement = serialized.data
        chargement_id = processing_chargement['id']
        headers_existing = processing_chargement['presence_entete']
        file_url = processing_chargement['fichier_chargement']
        encodage = processing_chargement['encodage']
        delimiter = processing_chargement['caratere_separation']
        if headers_existing:
            return Response({'message':'Opération de Mapping non autorisée sur un fichier possédant des entêtes'})
        data = request.data
        fieldnames = [x for x in data.values()]
        doMapping.delay(chargement_id, fieldnames)
        return Response({'recieved':request.data})

#OK
    @action(detail=True, methods=['GET'], url_path='checking-mandatory-fields')
    def checking_fields(self, request, pk=None):
        """Chekh if mandatory fields are correctely set on csv file before uploading"""
        chargement = self.get_object()
        serialized = serializers.MappingSerializer(chargement)
        processing_chargement = serialized.data
        csv_link = processing_chargement['fichier_chargement']
        delimiter = processing_chargement['caratere_separation']
        with urllib.request.urlopen('http://localhost:8000'+csv_link) as response:
            with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                    shutil.copyfileobj(response, tmp_file)
            with open(tmp_file.name) as file_csv:
                csv_dict_reader = DictReader(file_csv)
                headers = csv_dict_reader.fieldnames
                for row in csv_dict_reader:
                    if row['client_name']=="":
                        return Response({'message':'Vérifier que les champs "client_name sont" remplis, puis réessayer'})
                    else:
                        return Response({'message':'Champs remplis'})
#OK
    @action(detail=True, methods=['GET'], url_path='summary')
    def summary_fields(self, request, pk=None):
        chargement = self.get_object()
        serialized = serializers.MappingSerializer(chargement)
        processing_chargement = serialized.data
        csv_link = processing_chargement['fichier_chargement']
        with urllib.request.urlopen('http://localhost:8000'+csv_link) as response:
            with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                    shutil.copyfileobj(response, tmp_file)
            with open(tmp_file.name) as file_csv:
                csv_dict_reader = DictReader(file_csv)
                referenced_data =[]
                for row in csv_dict_reader:
                    referenced_data.append(row)
                return Response({'data':referenced_data[:5]})
                
    
    @action(detail=True, methods=['POST'], url_path='finish-confirm', serializer_class=serializers.TagPaySerializer)
    def confirm_finish(self, request, pk=None):
        """Start Loading data from CSV files to TAGPAY ENDPOINT"""
        chargement = self.get_object()
        serialized = serializers.MappingSerializer(chargement)
        processing_data = serialized.data
        chargement_id = processing_data['id']
        TokenRefresh.load_csv_data.delay(chargement_id)
        return Response({'test':'OK', 'chargement_id':chargement_id})



class ChargementForm(forms.ModelForm):
    class Meta:
        model = Chargement
        exclude = []
        widgets = {
            'id': DBClearableFileInput
        }