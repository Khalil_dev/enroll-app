from django.apps import AppConfig


class ChargementConfig(AppConfig):
    name = 'chargement'
