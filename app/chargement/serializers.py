from rest_framework import serializers
from core.models import Chargement, Plateforme, ProfileClient


class ChargementSerializer(serializers.ModelSerializer):
    """ Create a serializer for chargement """
    
    class Meta:
        model = Chargement
        fields = ('id','plateforme','profil','encodage','presence_entete','caratere_separation','caratere_protection','fichier_chargement')
        read_only_fields = ('id',)


class MappingSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = Chargement
        fields = "__all__"
        read_only_fields = ('id',)


class TagPaySerializer(serializers.Serializer):

    TAGPAY_FIELDS = {
        ('client_id', 'client_id'),
        ('client_status','client_status'),
        ('client_name', 'client_name'),
        ('client_firstname', 'client_firstname'),
        ('client_postname', 'client_postname'),
        ('client_msisdn','client_msisdn'),
        ('client_balance','client_balance'),
        ('client_cos', 'client_cos')
    }

    """ Serializing Tagpay Fields"""
    Champ_1 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_2 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_3 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_4 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_5 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_6 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_7 = serializers.ChoiceField(choices=TAGPAY_FIELDS)
    Champ_8 = serializers.ChoiceField(choices=TAGPAY_FIELDS)






    


    