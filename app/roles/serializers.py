from rest_framework import serializers
from core.models import Role


class RoleSerializer(serializers.ModelSerializer):
    """ Create a serializer for roles """

    class Meta:
        model = Role
        fields = "__all__"
        get_id_display= ('id')

