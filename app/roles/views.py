from rest_framework import viewsets, mixins
from core.models import Role
from roles import serializers


class RoleViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    ):
    """ Role viewset """
    queryset = Role.objects.all()
    serializer_class = serializers.RoleSerializer
