# YUP ENROLL DOCKER FILE
# CREATED : 08/04/2020
# AUTHOR: Ibrahima @ APPS TEAM - SYLVERSYS CONSULTING INTERNATIONAL

#Python Version used in this project
FROM python:3.7-alpine

LABEL maintainer="Apps Team @ SYLVERSYS-CONSULTING"

#Indicate python to run on unbuffered mode
# Recommended when python is running on a container
#Help avoid troubleshot on running
ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"


#AWS Configuration
ENV AWS_ACCESS_KEY_ID NOT_SET
ENV AWS_SECRET_ACCESS_KEY NOT_SET
ENV AWS_DEFAULT_REGION NOT_SET
ENV AWS_PARAMETER_STORE NOT_SET

RUN adduser -D yup
VOLUME ["/etc/nginx/yupssl"]

# Copying required dependencies file on docker image
COPY ./requirements.txt /requirements.txt


#Install temporary dependencies
RUN apk add --update --no-cache postgresql-client \
    && apk add --update --no-cache --virtual .tmp-build-deps \
        gcc libc-dev linux-headers postgresql-dev \
    && apk add gcc musl-dev libffi-dev openssl-dev python3-dev \
    && apk add --no-cache --virtual .pynacl_deps build-base \
    && pip install --no-cache-dir -U paramiko pynacl \
    && apk add --update --no-cache nginx \
    && apk add --update --no-cache unzip \
    && apk add --update --no-cache curl \
    && pip install -r /requirements.txt \
    && apk del .tmp-build-deps \
    && rm /requirements.txt

# install aws -cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install \
    && rm -f awscliv2.zip \
    && rm -rf ./aws

# Creating a directory into docker image to store source code
RUN mkdir /app
WORKDIR /app
COPY ./app /app
COPY ./scripts /scripts
RUN chown -R yup /app
# COPY ./entrypoint.sh /
# RUN chown yup /entrypoint.sh && chmod +x /entrypoint.sh
RUN chmod +x /scripts

# copy nginx conf
# COPY ./nginx.conf /etc/nginx/sites-enabled/yup-enroll.conf

#Switching to this user
USER yup
# ENTRYPOINT ["/entrypoint.sh"]
CMD ["entrypoint.sh"]
