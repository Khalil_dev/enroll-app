#!/bin/sh
#!/bin/bash

aws configure set region $AWS_DEFAULT_REGION
#aws ssm get-parameter --name "$AWS_PARAMETER_STORE" --with-decryption > /app/aws_param_store.json
set -e
cd /app
python manage.py wait_for_db && python manage.py migrate && python manage.py runserver 0.0.0.0:8000
